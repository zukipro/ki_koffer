import cv2
import numpy as np


# script to apply/test camera calibration results

matrix = [[818.50524436, 0.0, 716.31531958], [0.0, 818.50524436, 638.66807577], [0, 0, 1]]
distortion = [[1.87976406e+00],[1.40067691e+01], [-1.09650499e-03], [9.86541024e-04], [-2.31280843e+00],[2.19050876e+00],
              [1.50468667e+01],[9.80510634e-01],[0.00000000e+00],[0.00000000e+00],[0.00000000e+00],[0.00000000e+00],
              [0.00000000e+00],[0.00000000e+00]]

mtx = np.asarray(matrix)
dist = np.asarray(distortion)

img = cv2.imread('Bild_0.png')
h,  w = img.shape[:2]
newcameramtx, roi = cv2.getOptimalNewCameraMatrix(mtx, dist, (w, h), 0, (w, h))

# undistort
dst = cv2.undistort(img, mtx, dist, None, newcameramtx)

# crop the image
x, y, w, h = roi
dst = dst[y:y+h, x:x+w]
cv2.imwrite('CAM3_calibresult.png', dst)
print(newcameramtx)

