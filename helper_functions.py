# import dependencies
import numpy as np
import tensorflow as tf
import pandas as pd
import matplotlib.pyplot as plt
import tensorflow as tf
import shutil
import cv2
import pickle
import time


def order_points(pts):
    """
    initialzie a list of coordinates that will be ordered
    such that the first entry in the list is the top-left,
    the second entry is the top-right, the third is the
    bottom-right, and the fourth is the bottom-left

    function taken from
    https://pyimagesearch.com/2014/08/25/4-point-opencv-getperspective-transform-example/

    :param pts: list of corner points
    :return: ordered list of rectangle corner points
    """
    rect = np.zeros((4, 2), dtype="float32")
    # the top-left point will have the smallest sum, whereas
    # the bottom-right point will have the largest sum
    s = pts.sum(axis=1)
    rect[0] = pts[np.argmin(s)]
    rect[2] = pts[np.argmax(s)]
    # now, compute the difference between the points, the
    # top-right point will have the smallest difference,
    # whereas the bottom-left will have the largest difference
    diff = np.diff(pts, axis=1)
    rect[1] = pts[np.argmin(diff)]
    rect[3] = pts[np.argmax(diff)]
    # return the ordered coordinates
    return rect


def four_point_transform(image, pts):
    """
    function to transform image within four corners into a rectangle
    image is warped accordingly

    function taken from
    https://pyimagesearch.com/2014/08/25/4-point-opencv-getperspective-transform-example/

    :param image: image that should be transformed
    :param pts: coordinates of corners of region of interest
    :return: warped image
    """
    # obtain a consistent order of the points and unpack them
    # individually
    rect = order_points(pts)
    (tl, tr, br, bl) = rect
    # compute the width of the new image, which will be the
    # maximum distance between bottom-right and bottom-left
    # x-coordiates or the top-right and top-left x-coordinates
    widthA = np.sqrt(((br[0] - bl[0]) ** 2) + ((br[1] - bl[1]) ** 2))
    widthB = np.sqrt(((tr[0] - tl[0]) ** 2) + ((tr[1] - tl[1]) ** 2))
    maxWidth = max(int(widthA), int(widthB))
    # compute the height of the new image, which will be the
    # maximum distance between the top-right and bottom-right
    # y-coordinates or the top-left and bottom-left y-coordinates
    heightA = np.sqrt(((tr[0] - br[0]) ** 2) + ((tr[1] - br[1]) ** 2))
    heightB = np.sqrt(((tl[0] - bl[0]) ** 2) + ((tl[1] - bl[1]) ** 2))
    maxHeight = max(int(heightA), int(heightB))
    # now that we have the dimensions of the new image, construct
    # the set of destination points to obtain a "birds eye view",
    # (i.e. top-down view) of the image, again specifying points
    # in the top-left, top-right, bottom-right, and bottom-left
    # order
    dst = np.array([
        [0, 0],
        [maxWidth - 1, 0],
        [maxWidth - 1, maxHeight - 1],
        [0, maxHeight - 1]], dtype = "float32")
    # compute the perspective transform matrix and then apply it
    M = cv2.getPerspectiveTransform(rect, dst)
    warped = cv2.warpPerspective(image, M, (maxWidth, maxHeight))
    # return the warped image
    return warped


def preprocess(map1, map2, corner_points, frame):
    """
    cropping and reducing distortion of image

    :param map1: map containing the camera_calibration distortion
    :param map2: map containing the camera_calibration distortion
    :param corner_points: corner points of region of interest
    :param frame: streamed image
    :return: preprocessed image
    """
    # undistort image
    # use remap function for real time capability
    dst = cv2.remap(frame, map1, map2, interpolation=cv2.INTER_LINEAR, borderMode=cv2.BORDER_REFLECT_101,
                     borderValue=None)
    # cut image to avoid problems with white outside the demonstrator
    dst = dst[100:dst.shape[0] - 200, 0:dst.shape[1]]

    # warp image between four corner points
    warped = four_point_transform(dst, corner_points)

    return warped


def find_white(frame):
    """
    function for camera calibration
    :param frame:
    :return:
    """

    for i in range(250, 60, -10):
        # boundaries for color detection
        lower = [i, i, i]
        upper = [255, 255, 255]
        # create NumPy arrays from the boundaries
        lower = np.array(lower, dtype="uint8")
        upper = np.array(upper, dtype="uint8")

        # find the colors within the specified boundaries and apply
        # the mask
        mask = cv2.inRange(frame, lower, upper)
        output = cv2.bitwise_and(frame, frame, mask=mask)

        # divide image in four different regions to find seperate lego bricks
        mask_1 = mask[0:int(mask.shape[0] / 2), 0:int(mask.shape[1] / 2)]
        mask_2 = mask[0:int(mask.shape[0] / 2), int(mask.shape[1] / 2):mask.shape[1]]
        mask_3 = mask[int(mask.shape[0] / 2):mask.shape[0], 0:int(mask.shape[1] / 2)]
        mask_4 = mask[int(mask.shape[0] / 2):mask.shape[0], int(mask.shape[1] / 2):mask.shape[1]]


        # check if enough white pixels are found in each quarter
        if (np.count_nonzero(mask_1 == 255) > 1000 and np.count_nonzero(mask_2 == 255) > 1000 and
                np.count_nonzero(mask_3 == 255) > 1000 and np.count_nonzero(mask_4 == 255) > 1000):
            # find and mark the different corner points
            point_1 = np.where(mask_1 == 255)
            point_1_x = int(point_1[0].mean())
            point_1_y = int(point_1[1].mean())
            #frame = cv2.circle(frame, (point_1_y, point_1_x), 5, (255, 0, 0), 5)

            point_2 = np.where(mask_2 == 255)
            point_2_x = int(point_2[0].mean())
            point_2_y = int(point_2[1].mean()) + int(mask.shape[1] / 2)
            #frame = cv2.circle(frame, (point_2_y, point_2_x), 5, (255, 0, 0), 5)

            point_3 = np.where(mask_3 == 255)
            point_3_x = int(point_3[0].mean()) + int(mask.shape[0] / 2)
            point_3_y = int(point_3[1].mean())
            #frame = cv2.circle(frame, (point_3_y, point_3_x), 5, (255, 0, 0), 5)

            point_4 = np.where(mask_4 == 255)
            point_4_x = int(point_4[0].mean()) + + int(mask.shape[0] / 2)
            point_4_y = int(point_4[1].mean()) + int(mask.shape[1] / 2)
            #frame = cv2.circle(frame, (point_4_y, point_4_x), 5, (255, 0, 0), 5)

            corner_points = np.array([(point_1_y - 80, point_1_x - 80), (point_2_y + 80, point_2_x - 80),
                                      (point_3_y - 80, point_3_x + 80), (point_4_y + 80, point_4_x + 80)])

            box_points = np.array([(point_1_y, point_1_x), (point_2_y, point_2_x),
                                   (point_3_y, point_3_x), (point_4_y, point_4_x)])

            # calculate warped image
            warped = four_point_transform(frame.copy(), corner_points)

            # break out of for loop
            break

    return warped, corner_points, box_points


def calibrate_right():
    """
    function for camera calibration
    :return:
    """
    cap = cv2.VideoCapture('http://169.254.58.155:8000/stream.mjpg')

    frame_count = 0

    while frame_count < 5:
        frame_count += 1
        ret, frame = cap.read()

        # crop image
        frame = frame[500:950, 500:1000]
        test_frame = frame.copy()
        # find white bricks
        warped, corner_points, _ = find_white(frame)
        _, _, box_points = find_white(warped)

    for i in range(4):
        warped = cv2.circle(warped, box_points[i], 5, (255, 0, 0), 5)

    return box_points, warped, corner_points



def calibrate():
    """
    calibrate corner points by putting white bricks in the corners
    :return: corner_points; list of new corner points
    :return: warped; warped image

    """
    # open video stream
    cap = cv2.VideoCapture('http://169.254.242.185:8000/stream.mjpg')

    # camera_calibration parameters
    matrix = [[818.50524436, 0.0, 716.31531958], [0.0, 818.50524436, 638.66807577], [0, 0, 1]]
    distortion = [[1.87976406e+00], [1.40067691e+01], [-1.09650499e-03], [9.86541024e-04], [-2.31280843e+00],
                  [2.19050876e+00],
                  [1.50468667e+01], [9.80510634e-01], [0.00000000e+00], [0.00000000e+00], [0.00000000e+00],
                  [0.00000000e+00],
                  [0.00000000e+00], [0.00000000e+00]]

    # transform to array
    mtx = np.asarray(matrix)
    dist = np.asarray(distortion)

    # undistort
    map1, map2 = cv2.initUndistortRectifyMap(mtx, dist, None, None, (1232, 1640), cv2.CV_32FC1)

    # wait some time for camera_calibration for more stable results
    frame_count = 0

    while frame_count < 5:
        frame_count += 1
        ret, frame = cap.read()

        #undistort image
        dst = cv2.remap(frame, map1, map2, interpolation=cv2.INTER_LINEAR, borderMode=cv2.BORDER_REFLECT_101,
                        borderValue=None)

        frame = dst

        # crop image
        frame = frame[100:frame.shape[0] - 200, 0:frame.shape[1]]

        # find white bricks
        for i in range(250, 60, -10):
            # boundaries for color detection
            lower = [i, i, i]
            upper = [255, 255, 255]
            # create NumPy arrays from the boundaries
            lower = np.array(lower, dtype="uint8")
            upper = np.array(upper, dtype="uint8")

            # find the colors within the specified boundaries and apply
            # the mask
            mask = cv2.inRange(frame, lower, upper)
            output = cv2.bitwise_and(frame, frame, mask=mask)

            # divide image in four different regions to find seperate lego bricks
            mask_1 = mask[0:int(mask.shape[0] / 2), 0:int(mask.shape[1] / 2)]
            mask_2 = mask[0:int(mask.shape[0] / 2), int(mask.shape[1] / 2):mask.shape[1]]
            mask_3 = mask[int(mask.shape[0] / 2):mask.shape[0], 0:int(mask.shape[1] / 2)]
            mask_4 = mask[int(mask.shape[0] / 2):mask.shape[0], int(mask.shape[1] / 2):mask.shape[1]]

            # check if enough white pixels are found in each quarter
            if (np.count_nonzero(mask_1 == 255) > 1000 and np.count_nonzero(mask_2 == 255) > 1000 and
                    np.count_nonzero(mask_3 == 255) > 1000 and np.count_nonzero(mask_4 == 255) > 1000):

                # find and mark the different corner points
                point_1 = np.where(mask_1 == 255)
                point_1_x = int(point_1[0].mean())
                point_1_y = int(point_1[1].mean())
                frame = cv2.circle(frame, (point_1_y, point_1_x), 5, (255, 0, 0), 5)

                point_2 = np.where(mask_2 == 255)
                point_2_x = int(point_2[0].mean())
                point_2_y = int(point_2[1].mean()) + int(mask.shape[1] / 2)
                frame = cv2.circle(frame, (point_2_y, point_2_x), 5, (255, 0, 0), 5)

                point_3 = np.where(mask_3 == 255)
                point_3_x = int(point_3[0].mean()) + int(mask.shape[0] / 2)
                point_3_y = int(point_3[1].mean())
                frame = cv2.circle(frame, (point_3_y, point_3_x), 5, (255, 0, 0), 5)

                point_4 = np.where(mask_4 == 255)
                point_4_x = int(point_4[0].mean()) + + int(mask.shape[0] / 2)
                point_4_y = int(point_4[1].mean()) + int(mask.shape[1] / 2)
                frame = cv2.circle(frame, (point_4_y, point_4_x), 5, (255, 0, 0), 5)

                corner_points = np.array([(point_1_y - 80, point_1_x - 80), (point_2_y + 80, point_2_x - 80),
                                      (point_3_y - 80, point_3_x + 80), (point_4_y + 80, point_4_x + 80)])

                # calculate warped image
                warped = four_point_transform(frame.copy(), corner_points)
                # break out of for loop
                break

    return corner_points, warped


def draw_lines(img):
    """
    draw lines on image that show the region of each box
    :param img: preprocessed image from the camera
    :return: image with lines indicating the box positions
    """
    for i in range(1, 6):
        cv2.line(img, (0, int((img.shape[0]) / 6 * i)), (img.shape[1],
                 int((img.shape[0]) / 6 * i)), (0, 0, 255), 10)
    for i in range(1, 5):
        cv2.line(img, (int((img.shape[1]) / 4 * i), 0), (int((img.shape[1]) / 4 * i),
                 img.shape[0]), (0, 0, 255), 10)

    return img


def draw_boxes_right(frame, box_points_right, defects):


    x1 = box_points_right[0, 0]
    x2 = box_points_right[3, 0]
    x3 = int(x1 + (x2 - x1)/3)
    x4 = int(x1 + (x2 - x1) / 3 * 2)

    y1 = box_points_right[0, 1]
    y2 = box_points_right[2, 1]
    y3 = int(y1 + (y2 - y1)/2)

    X = (x1, x2, x3, x4)
    Y = (y1, y2, y3)

    count = 0
    if len(defects) != 0:
        for x in X:
            for y in Y:
                #print(defects[count])
                if defects[count] == 0:
                    cv2.rectangle(frame,
                                  (x - 35, y - 35),
                                  (x + 35, y + 35),
                                  (0, 0, 255),
                                  4)
                elif defects[count] == 1:
                    cv2.rectangle(frame,
                                  (x - 35, y - 35),
                                  (x + 35, y + 35),
                                  (0, 255, 0),
                                  4)
                count += 1


    return frame


    #for col in range(3):
    #    for row in range(4):



def draw_squares(img, model_place, predict, prediction):
    """
    draw red or green rectangles depending on free spaces for boxes
    :param img: preprocessed camera image
    :param model_place: model to finde free spaces
    :param predict: boolean whether new predictions should take place
    :param prediction: array containing the predictions of free spaces
    :return: annotated image
    """
    section_all = []
    count = 0
    time_sum = 0
    for i in range(1, 7):
        for j in range(1, 5):

            section = img[int((img.shape[0]) / 6 * (i - 1)):int((img.shape[0]) / 6 * i),
                          int((img.shape[1]) / 4 * (j - 1)):int((img.shape[1]) / 4 * j)]

            section = cv2.resize(section, (210, 150), interpolation=cv2.INTER_LINEAR)


            section = cv2.cvtColor(section, cv2.COLOR_BGR2RGB)
            section = section.reshape(-1, 150, 210, 3)
            section = section / 255

            section_all.append(section)

            if prediction[count] == 1:
                cv2.rectangle(img,
                              (int((img.shape[1]) / 4 * (j - 1)) + 20, int((img.shape[0]) / 6 * (i - 1)) + 20),
                              (int((img.shape[1]) / 4 * j) - 20, int((img.shape[0]) / 6 * i) - 20),
                              (0, 255, 0),
                              10)
            else:
                cv2.rectangle(img,
                              (int((img.shape[1]) / 4 * (j - 1)) + 20, int((img.shape[0]) / 6 * (i - 1)) + 20),
                              (int((img.shape[1]) / 4 * j) - 20, int((img.shape[0]) / 6 * i) - 20),
                              (0, 0, 255),
                              10)

            count += 1

    if predict:
        prediction = (np.round(model_place.predict(np.vstack(section_all),batch_size=24)))
    return img, prediction


def get_calibration():
    """
    load camera_calibration data

    :return: four integers of positions of boxes and empty positions
    """
    matrix = [[818.50524436, 0.0, 716.31531958], [0.0, 818.50524436, 638.66807577], [0, 0, 1]]
    distortion = [[1.87976406e+00], [1.40067691e+01], [-1.09650499e-03], [9.86541024e-04], [-2.31280843e+00],
                  [2.19050876e+00],
                  [1.50468667e+01], [9.80510634e-01], [0.00000000e+00], [0.00000000e+00], [0.00000000e+00],
                  [0.00000000e+00],
                  [0.00000000e+00], [0.00000000e+00]]

    mtx = np.asarray(matrix)
    dist = np.asarray(distortion)

    map1, map2 = cv2.initUndistortRectifyMap(mtx, dist, None, None, (1232, 1640), cv2.CV_32FC1)

    open_file = open("calibration_data/corner_points_1", "rb")
    corner_points = pickle.load(open_file)
    open_file.close()

    return map1, map2, corner_points


def get_color(map1, map2, corner_points):

    cap = cv2.VideoCapture('http://169.254.242.185:8000/stream.mjpg')
    model = tf.keras.models.load_model('./models/train_color_5.h5')

    output = list(range(24))
    count = 0

    while True:
        count = 0
        ret, frame = cap.read()
        frame = preprocess(map1, map2, corner_points, frame)
        test = frame.copy()

        for i in range(1, 7):
            for j in range(1, 5):
                section = frame[int((frame.shape[0]) / 6 * (i - 1)):int((frame.shape[0]) / 6 * i),
                          int((frame.shape[1]) / 4 * (j - 1)):int((frame.shape[1]) / 4 * j)]

                section = cv2.resize(section, (210, 150), interpolation=cv2.INTER_AREA)
                # section = cv2.resize(section, (210, 150), interpolation=cv2.INTER_AREA)
                section = cv2.cvtColor(section, cv2.COLOR_BGR2RGB)

                #section = section.reshape(-1, 80, 80, 3)
                section = section.reshape(-1, 150, 210, 3)
                section = section / 255

                output[count] = model.predict(section)
                count += 1

        break


    cap.release()

    return output



def write_colors(img, classes, output_colors):
    count = 0
    for i in range(1, 7):
        for j in range(1, 5):
            x_coord = int((img.shape[0]) / 6 * (i - 1)) + int((img.shape[0]) / 6 * i)
            x_coord = x_coord / 2
            x_coord = int(x_coord)

            y_coord = int((img.shape[1]) / 4 * (j - 1))
            # y_coord = y_coord/2
            # y_coord = int(y_coord)

            color_temp = output_colors[count][0]
            indices = np.where(color_temp == color_temp.max())
            indices = int(indices[0])
            count += 1
            text = classes[indices]

            if "Platte" in text:
                text = text.replace('Platte', '')
                cv2.putText(img, "Platte", (y_coord + 30, x_coord + 50),
                            cv2.FONT_HERSHEY_SIMPLEX,
                            1.4, (255, 0, 0), 4)

            cv2.putText(img, text, (y_coord + 30, x_coord),
                        cv2.FONT_HERSHEY_SIMPLEX,
                        1.4, (255, 0, 0), 4)

            #cv2.putText(img, str(count), (y_coord, x_coord),
            #            cv2.FONT_HERSHEY_SIMPLEX,
            #            2.0, (255, 0, 0), 3)


    return img


def reindex(index):
    """
    function to index color array in the same way as lights
    [3, 7, 11, 15, 19, 23   -->     [0, 1, 2, 3, 4, 5,
     2, 6, 10, 14, 18, 22            6, 7, 8, 9, 10, 11,
     1, 5, 9, 13, 17, 21             12, 13, 14, 15, 16, 17
     0, 4, 8, 12, 16, 20]            18, 19, 20, 21, 22, 23]

    :param index:
    :return:
    """

    index = np.reshape(index, (4, 6), order="F")

    ordered = np.zeros((4, 6))

    count = 0
    for i in range(3, -1, -1):
        ordered[count, :] = index[i, :]
        count += 1

    ordered = np.reshape(ordered, (24))
    return(ordered.astype(int))

def preprocess_lights(map1, map2, corner_points, frame):
    """
    cropping and reducing distortion of image

    :param map1: map containing the camera_calibration distortion
    :param map2: map containing the camera_calibration distortion
    :param corner_points: corner points of region of interest
    :param frame: streamed image
    :return: preprocessed image
    """
    # undistort image
    # use remap function for real time capability
    dst = cv2.remap(frame, map1, map2, interpolation=cv2.INTER_LINEAR, borderMode=cv2.BORDER_REFLECT_101,
                     borderValue=None)
    # cut image to avoid problems with white outside the demonstrator
    dst = dst[100:dst.shape[0] - 200, 0:dst.shape[1]]

    corner_points_2 = corner_points.copy()
    corner_points_2[0, 0] = corner_points[0, 0] -50
    corner_points_2[2, 0] = corner_points[2, 0] -50

    # warp image between four corner points
    warped = four_point_transform(dst, corner_points_2)

    return warped





