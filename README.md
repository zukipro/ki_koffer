# Demonstrating Computer Vision to Small- and Medium-sized Enterprises in Manufacturing: Towards Overcoming Costs and Implementation Challenges

This repository contains the codebase for the implementation discussed in the publication titled "Demonstrating Computer Vision to Small- and Medium-sized Enterprises in Manufacturing: Towards Overcoming Costs and Implementation Challenges" The associated research explores the development of a simple computer vision demonstrator and its demonstration to small- and medium-sized enterprises.

## Abstract

Computer vision (CV) systems are crucial in various fields, such as production technology. However, the general feedback from Small and Medium-Sized Enterprises (SMEs) in manufacturing indicates that implementing CV systems are out of reach due to their high costs, the need for expertise, and the complexities of Machine Learning (ML). To demonstrate the feasibility and applicability in SMEs, we present cost-effective portable CV-based demonstrator for SMEs in manufacturing. This paper shows that building upon existing technologies and methods, combining an off-the-shelf microcontroller for camera control with a single-board computer for signal processing, is practically achievable. These low-cost components incorporate open-source software for advanced ML and a user-friendly graphical interface. To widen the possible utilization of the system, Transfer Learning was adapted, leveraging pre-existing ML models. We present two illustrative use-cases with fault detection and inventory management based on plastic brick datasets including various colors and shapes to demonstrate the system’s effectiveness. Bringing this into a showable demonstrator, all components are integrated into a portable suitcase as a plug and play demonstration. We showcase the portable demonstrator at four different industrial fairs, leveraging this dynamic platform for direct interaction with SMEs. Firsthand insights and feedback from SMEs regarding our demonstration and their challenges, as well as opportunities of CV in manufacturing were received and summarized in this research.

## KEYWORDS

Computer Vision, Machine Vision, Manufacturing, Fault Detection, Inventory Management, SME

## Citation

If you find this code useful for your research, please consider citing our publication:

@article{authorYear,
  title={Demonstrating Computer Vision to Small- and Medium-sized Enterprises in Manufacturing: Towards Overcoming Costs and Implementation Challenges},
  author={Jonas Werheid, Sven Münker, Nils Klasen, Tobias Hamann, Anas Abdelrazeq, Robert Schmitt},
  journal={Journal Name},
  year={2023},
  doi={DOI},
}

## License

MIT License

Copyright (c) 2023 werheid, j; et al.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

## Contact

j.werheid@wzl-mq.rwth-aachen.de