import pigpio
from time import sleep


def shut_down_Pins():
    """
    function to reset PINs and thereby turn of all LEDs
    :return:
    """
    # list of used GPIO PINs on the PI
    PINS = [4, 17, 18, 23, 24, 25, 12, 16]

    # IP-adress of the PI connected to LEDs and Port
    # used to connect to PI
    pi = pigpio.pi('169.254.58.155', 8888)

    # loop to reset all PINs
    for k in PINS:
        pi.set_mode(k, pigpio.INPUT)


def light_PIN(x):
    """
    light LED x
    :param x: integer of LED to be lit
    :return:
    """
    # dict to map LEDs to corresponding PINs
    pin_dict = {3: [4, 18],
                2: [4, 23],
                1: [4, 24],
                5: [4, 25],
                6: [4, 12],
                4: [4, 16],
                9: [18, 4],
                8: [23, 4],
                7: [24, 4],
                11: [25, 4],
                12: [12, 4],
                10: [16, 4],
                15: [17, 18],
                14: [17, 23],
                13: [17, 24],
                17: [17, 25],
                18: [17, 12],
                16: [17, 16],
                21: [18, 17],
                20: [23, 17],
                19: [24, 17],
                23: [25, 17],
                24: [12, 17],
                22: [16, 17]}

    # list of used GPIO PINs on the PI
    PINS = [4, 17, 18, 23, 24, 25, 12, 16]

    # IP-adress of the PI connected to LEDs and Port
    # used to connect to PI
    pi = pigpio.pi('169.254.58.155', 8888)

    # light LED
    for k in PINS:
        pi.set_mode(k, pigpio.INPUT)

    pi.write(pin_dict[x][1], 0)
    pi.write(pin_dict[x][0], 1)




