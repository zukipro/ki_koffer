# import kivy packages
from kivy.app import App
from kivy.core.window import Window
from kivy.uix.widget import Widget
from kivy.lang import Builder
from kivy.uix.image import Image
from kivy.uix.button import Button
from kivy.clock import Clock
from kivy.graphics.texture import Texture
from kivy.properties import ObjectProperty
from kivy.uix.screenmanager import Screen, ScreenManager
from kivy.config import Config
from kivy.uix.vkeyboard import VKeyboard
from kivy.uix.popup import Popup
from kivy.uix.relativelayout import RelativeLayout


# import other packages
import cv2 as cv
import pickle
import tensorflow as tf
import numpy as np
from tensorflow.keras.models import load_model
import time


# custom functions
from helper_functions import *
from gpio2 import light_PIN, shut_down_Pins


# global variables
global corner_points
global calib_image
global map1, map2
global box_points_right
global corner_points_right

# dictionary to map classes to index of DNN
classes = {0: 'beige',
           1:  'beige Platte',
           2: 'blau-grau',
           3: 'dunkel blau',
           4: 'hell blau',
           5: 'dunkel blau Platte',
           6: 'braun ',
           7: 'braun Platte',
           8: 'gelb',
           9: 'gelb Platte',
           10: 'dunkel grau Platte',
           11: 'hell grau Platte',
           12: 'dunkel gruen Platte',
           13: 'dunkel gruen',
           14: 'hell gruen',
           15: 'hell gruen Platte',
           16: 'olive Platte',
           17: 'orange',
           18: 'orange Platte',
           19: 'rot',
           20: 'rot Platte',
           21: 'tuerkis Platte',
           22: 'weiss',
           23: 'weiss Platte'}


class PopUp(Popup):
    """
    popub class, popup window for camera_calibration
    """

    second = False

    def show_img(self):
        """
        draw image on popup window
        :return:
        """
        # show calibrated image including lines that divide sections
        if not self.second:
            img = draw_lines(calib_image)
            img = cv.rotate(img, cv.ROTATE_90_COUNTERCLOCKWISE)

            # output image on kivy gui
            buf = cv.flip(img, 0).tobytes()
            image_texture = Texture.create(size=(img.shape[1], img.shape[0]), colorfmt='bgr')
            image_texture.blit_buffer(buf, colorfmt='bgr', bufferfmt='ubyte')
            self.ids.img.texture = image_texture

    def switch_second(self):
        """
        switch to calibration of second camera
        similar procedure to first stream
        :return:
        """

        global box_points_right
        global corner_points_right

        box_points_right, calib_image_right, corner_points_right = calibrate_right()


        img = cv.rotate(calib_image_right, cv.ROTATE_90_CLOCKWISE)

        # output image on kivy gui
        buf = cv.flip(img, 0).tobytes()
        image_texture = Texture.create(size=(img.shape[1], img.shape[0]), colorfmt='bgr')
        image_texture.blit_buffer(buf, colorfmt='bgr', bufferfmt='ubyte')
        self.ids.img.texture = image_texture

    def recalibrate(self):
        """
        recalibrate on button click
        :return:
        """
        print(self.second)

        global calib_image
        global corner_points

        if not self.second:
            # call camera_calibration function
            corner_points, calib_image = calibrate()
            self.show_img(calib_image)

        elif self.second:
            self.switch_second()


    def save(self):
        """
        save new camera_calibration data on button click
        :return:
        """

        global box_points_right
        global corner_points_right

        if not self.second:
            # save file containing the corner points
            open_file = open("calibration_data/corner_points_1", "wb")
            pickle.dump(corner_points, open_file)
            open_file.close()

            self.switch_second()
            self.second = True

        elif self.second:
            open_file = open("calibration_data/corner_points_right", "wb")
            pickle.dump(box_points_right, open_file)
            open_file.close()

            open_file = open("calibration_data/corner_points_right", "wb")
            pickle.dump(corner_points_right, open_file)
            open_file.close()

            # close popup
            self.dismiss()

    def discard(self):
        """
        discard new camera_calibration and load old camera_calibration data
        :return:
        """
        global corner_points
        global box_points_right
        global corner_points_right

        if not self.second:

            # open previous camera_calibration file and close popup
            open_file = open("calibration_data/corner_points_1", "rb")
            corner_points = pickle.load(open_file)

            open_file.close()
            self.second = True
            self.switch_second()
        elif self.second:

            # open previous camera_calibration file and close popup
            open_file = open("calibration_data/box_points_right", "rb")
            box_points_right = pickle.load(open_file)
            open_file.close()

            # open previous camera_calibration file and close popup
            open_file = open("calibration_data/corner_points_right", "rb")
            corner_points_right = pickle.load(open_file)
            open_file.close()

            self.dismiss()


# define different Screens
class Window01(Screen):
    def button1_press(self):
        """
        calibrate camera (corner points) on button click
        :return:
        """

        global calib_image
        global corner_points

        corner_points, calib_image = calibrate()


class Window02(Screen):
    def button2_release(self):

        """
        estimate colors on button click
        :return:
        """

        global output_colors
        global output_colors_test

        # call function to get colors
        output_colors = get_color(map1, map2, corner_points)


class Window03(Screen):
    def button3_press(self):
        """
        estimate colors on button click
        :return:
        """

        global output_colors
        # call function to get colors
        output_colors = get_color(map1, map2, corner_points)


class Window04(Screen):
    pass


class Window05(Screen):
    pass


class Window06(Screen):
    def detect_defect(self):
        """
        function that outputs defective and defect free bricks with the right camera image
        :return: array of status (defect, no defect, no brick) for each of the 12 positions
        """

        global defects

        defects = []

        # load model
        model_defect = load_model('./models/train_defect_v3.h5')
        cap = cv2.VideoCapture('http://169.254.58.155:8000/stream.mjpg')
        ret, frame = cap.read()

        # take right image section
        frame = frame[500:950, 500:1000]
        frame = four_point_transform(frame.copy(), corner_points_right)

        # corner coordinates into frames for CNN
        x1 = box_points_right[0, 0]
        x2 = box_points_right[3, 0]
        x3 = int(x1 + (x2 - x1) / 3)
        x4 = int(x1 + (x2 - x1) / 3 * 2)

        y1 = box_points_right[0, 1]
        y2 = box_points_right[2, 1]
        y3 = int(y1 + (y2 - y1) / 2)

        X = (x1, x2, x3, x4)
        Y = (y1, y2, y3)

        # check CNN for each frame
        for x in X:
            for y in Y:
                img = frame[y-40:y+40, x-40:x+40]
                img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
                img = img/255
                img = img.reshape(-1, 80, 80, 3)
                temp = model_defect.predict(img)
                temp = np.argmax(temp)
                defects.append(temp)

        cap.release()


class Window07(Screen):
    pass


class Window08(Screen):
    pass


class Window09(Screen):
    pass


class Window10(Screen):
    pass


class Window11(Screen):
    pass


class Window12(Screen):
    pass


class Window13(Screen):
    pass


class WindowManager(ScreenManager):
    """
    Windowmanager class
    allows to handle different windows
    parent class of screen class
    """

    def update_label_05(self, i=6):
        """
        method to update the label and LED Pin
        :param i:
        :return:
        """

        global color_index

        # use singular in text if only one brick is to be picked
        if i == 1:
            self.get_screen("window_05").ids.label_4.text = f"""Bitte entnehmen Sie {i} Stein aus der markierten Kiste """
        else:
            self.get_screen("window_05").ids.label_4.text = f"""Bitte entnehmen Sie {i} Steine aus der markierten Kiste """

        # light LEDs
        try:
            index = []
            for count in range(24):
                color_temp = output_colors[count][0]
                indices = np.where(color_temp == color_temp.max())
                indices = int(indices[0])
                index.append(indices)

            index = reindex(index)
            index = np.array(index)
            color = (np.where(index == simpsons_dict[color_index][0])[0][0])

            light_PIN(color + 1)

        except:
            print("color does not exsist")
            shut_down_Pins()

        # next brick
        color_index += 1

    def button_5_press(self):
        """
        update LED Pins and labels on foreward button
        :return:
        """
        global color_index

        if color_index == len(simpsons_dict):
            self.current = "window_06"
            shut_down_Pins()
            color_index = 0
        else:
            self.update_label_05(simpsons_dict[color_index][1])


# load kivy files for different windows
Builder.load_file('GUI Windows/main_window.kv')
Builder.load_file('GUI Windows/popup.kv')
Builder.load_file('GUI Windows/window_01.kv')
Builder.load_file('GUI Windows/window_02.kv')
Builder.load_file('GUI Windows/window_03.kv')
Builder.load_file('GUI Windows/window_04.kv')
Builder.load_file('GUI Windows/window_05.kv')
Builder.load_file('GUI Windows/window_06.kv')
Builder.load_file('GUI Windows/window_07.kv')
Builder.load_file('GUI Windows/window_08.kv')
Builder.load_file('GUI Windows/window_09.kv')
Builder.load_file('GUI Windows/window_10.kv')
Builder.load_file('GUI Windows/window_11.kv')
Builder.load_file('GUI Windows/window_12.kv')
Builder.load_file('GUI Windows/window_13.kv')


class Demonstrator(App):
    """
    App class
    """
    def build(self):
        """
        build function that is called on build
        :return: self
        """
        # set icon for APP
        self.icon = "images/icon.png"

        global simpsons
        global simpsons_dict
        global color_index
        global corner_points
        global map1, map2
        global box_points_right
        global corner_points_right
        global defects

        # variable if Simpsons should be built
        # in case demonstrator should be extended to different classes
        simpsons = True

        # simpsons needed colors and amount of bricks
        simpsons_dict = {0: [8, 6],
                         1: [22, 2],
                         2: [3, 7],
                         3: [19, 1],
                         4: [17, 2],
                         5: [4, 1],
                         6: [14, 3]
                         }

        # starting index for color
        color_index = 0

        defects = []

        # get camera_calibration coordinates
        map1, map2, corner_points = get_calibration()

        # load model to show empty spaces
        self.model_place = load_model('./models/train_place.h5')

        # load right image box coordinates
        open_file = open("calibration_data/box_points_right", "rb")
        box_points_right = pickle.load(open_file)
        open_file.close()

        open_file = open("calibration_data/corner_points_right", "rb")
        corner_points_right = pickle.load(open_file)
        open_file.close()

        # call stream function
        self.stream()

        self.sm = WindowManager()
        return self.sm

    def on_stop(self):
        """"
        stops cv2 stream and shuts down Pins on kivy stop
        """
        shut_down_Pins()

        try:
            self.capture_1.release()
            self.capture_2.release()
        except:
            pass

    def stream(self):
        """
        cv2 video stream from camera
        """

        # video stream of left camera
        self.capture_1 = cv.VideoCapture('http://169.254.242.185:8000/stream.mjpg')

        # video stream of right camera
        self.capture_2 = cv.VideoCapture('http://169.254.58.155:8000/stream.mjpg')

        # list of empty/non empty spaces
        self.output_place = list(range(24))

        # framecount for camera stream
        self.count = 0

        # stream refresh rate
        Clock.schedule_interval(self.update, 1.0 / 30)

    def update(self, dt):
        """
        converts cv2 stream to kivy texture object to display as image
        """

        ret, frame = self.capture_1.read()
        ret2, frame2 = self.capture_2.read()

        # image stream depending on the window
        if self.sm.current == "window_02":
            # Stream in second window
            # show free spaces
            if ret:
                # boolean variable to indicate whether free spaces should be checked
                # only every n th frame to improve real-time capability since model evaluation currently
                # takes to much time

                predict = False
                if self.count % 30 == 0:
                    predict = True

                self.count += 1

                # reset frame count
                if self.count > 100000000:
                    self.count = 0

                # calculate right image section
                frame = preprocess(map1, map2, corner_points, frame)
                # find free spaces

                frame, self.output_place = draw_squares(frame, self.model_place, predict, self.output_place)

                # turn image in right direction
                frame = cv.rotate(frame, cv.ROTATE_90_COUNTERCLOCKWISE)

                # write image to Screen

                buf = cv.flip(frame, 0).tobytes()
                image_texture = Texture.create(size=(frame.shape[1], frame.shape[0]), colorfmt='bgr')
                image_texture.blit_buffer(buf, colorfmt='bgr', bufferfmt='ubyte')
                self.sm.get_screen("window_02").ids.vid.texture = image_texture

        elif self.sm.current == "window_03":
            # image stream for third window
            # color detection
            if ret:
                # call preprocessing function
                frame = preprocess(map1, map2, corner_points, frame)
                # image copy for annotations
                frame_copy = frame.copy()
                # convert color list to np array
                colors = np.array(output_colors)
                image = write_colors(frame_copy, classes, colors)

                # turn image in right direction
                image = cv.rotate(image, cv.ROTATE_90_COUNTERCLOCKWISE)

                # write image to Screen
                buf = cv.flip(image, 0).tobytes()
                image_texture = Texture.create(size=(image.shape[1], image.shape[0]), colorfmt='bgr')
                image_texture.blit_buffer(buf, colorfmt='bgr', bufferfmt='ubyte')
                self.sm.get_screen("window_03").ids.vid.texture = image_texture

        elif self.sm.current == "window_05":
            if ret:
                # preprocess and turn image
                frame = preprocess_lights(map1, map2, corner_points, frame)
                frame = cv.rotate(frame, cv.ROTATE_90_COUNTERCLOCKWISE)

                # write image to Screen
                buf = cv.flip(frame, 0).tobytes()
                image_texture = Texture.create(size=(frame.shape[1], frame.shape[0]), colorfmt='bgr')
                image_texture.blit_buffer(buf, colorfmt='bgr', bufferfmt='ubyte')
                self.sm.get_screen("window_05").ids.vid.texture = image_texture

        elif self.sm.current == "window_06":
            if ret2:
                # display right image
                # preprocess and turn image
                frame2 = frame2[500:950, 500:1000]
                frame2 = four_point_transform(frame2.copy(), corner_points_right)

                # get coordinates of the boxes and display them
                frame2 = draw_boxes_right(frame2, box_points_right, defects)

                # rotate image
                frame2 = cv.rotate(frame2, cv.ROTATE_90_CLOCKWISE)

                # write image to Screen
                buf = cv.flip(frame2, 0).tobytes()
                image_texture = Texture.create(size=(frame2.shape[1], frame2.shape[0]), colorfmt='bgr')
                image_texture.blit_buffer(buf, colorfmt='bgr', bufferfmt='ubyte')
                self.sm.get_screen("window_06").ids.vid.texture = image_texture



if __name__ == "__main__":
    Demonstrator().run()



