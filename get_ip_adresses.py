import io
import socket
import struct
from PIL import Image
import numpy as np
import matplotlib.pyplot as pl
import cv2


def get_ip_address():
    """
    function to return the ip addresses

    :return: IPAddr: Array of strings of ip-address
    """
    # sudo nano /etc/dhcpcd.conf

    ip_addresses = [i[4][0] for i in socket.getaddrinfo(socket.gethostname(), None)]
    ip_addr = []
    for addr in ip_addresses:
        if addr.startswith('192'):
            # print(addr)
            ip_addr.append(addr)
        elif addr.startswith('169'):
            # print(addr)
            ip_addr.append(addr)
    return ip_addr
