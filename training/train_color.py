# import dependencies
import numpy as np
import tensorflow as tf
import pandas as pd
import matplotlib.pyplot as plt
import tensorflow as tf
import shutil

# import deep learning dependencies
from tensorflow.keras.preprocessing.image import ImageDataGenerator , load_img ,img_to_array
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Activation, Dropout, Flatten, Dense, Conv2D, MaxPooling2D
from tensorflow.keras.callbacks import EarlyStopping
from sklearn.metrics import classification_report,confusion_matrix
from tensorflow.keras.optimizers import RMSprop
from tensorflow.keras.callbacks import EarlyStopping
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.applications.vgg16 import VGG16
from tensorflow.keras import layers, models

# define image shape and batch size
image_shape = (80, 80, 3)
batch_size = 3

# path to processed data
processed_dir = "./train_color/"

# load images
image_gen = ImageDataGenerator(rescale=1 / 255.0,
                               rotation_range=20,
                               zoom_range=0.1,
                               width_shift_range=0.1,
                               height_shift_range=0.1,
                               shear_range=0.1,
                               horizontal_flip=True,
                               validation_split=0.20,
                               brightness_range=(0.5, 1.5))


train_set = image_gen.flow_from_directory(processed_dir,
                                          target_size=image_shape[:2],
                                          color_mode="rgb",
                                          batch_size=batch_size,
                                          class_mode='categorical',
                                          subset='training',
                                          shuffle=True,
                                          seed=42)

valid_set = image_gen.flow_from_directory(processed_dir,
                                          target_size=image_shape[:2],
                                          color_mode="rgb",
                                          batch_size=batch_size,
                                          class_mode='categorical',
                                          subset='validation',
                                          shuffle=True,
                                          seed=42)

test = image_gen.flow_from_directory(processed_dir,
                                     target_size=image_shape[:2],
                                     color_mode="rgb",
                                     batch_size=batch_size,
                                     class_mode='categorical',
                                     shuffle=False)


# load VGG 16 with imagenet weights
pre_trained_model = VGG16(input_shape=(80, 80, 3),
                          include_top=False,
                          weights='imagenet')

# freeze layers
pre_trained_model.trainable = False

# add classification layers
flatten_layer = layers.Flatten()
dense_layer_1 = layers.Dense(128, activation='relu')
prediction_layer = layers.Dense(24, activation='softmax')
dropout = layers.Dropout(0.2)

# add classification layers and vgg
model = models.Sequential([
    pre_trained_model,
    flatten_layer,
    dense_layer_1,
    dropout,
    prediction_layer
])


# compile and train model
model.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=0.001),
              loss='categorical_crossentropy',
              metrics=['accuracy'])

history = model.fit(train_set,
                    validation_data=valid_set,
                    epochs=25)


model.save('train_color_1.h5')

# plot training acc
acc = history.history['accuracy']
val_acc = history.history['val_accuracy']
loss = history.history['loss']
val_loss = history.history['val_loss']

epochs = range(len(acc))

plt.plot(epochs, acc, 'r', label='Training accuracy')
plt.plot(epochs, val_acc, 'b', label='Validation accuracy')
plt.title('Training and validation accuracy')
plt.legend(loc=0)
plt.figure()
plt.show()


# plot confusion matrix
probabilities = model.predict(test)

predicted_classes = np.round(probabilities, decimals=0)
predicted_classes = predicted_classes.astype('int')
np.squeeze(predicted_classes)

#print(confusion_matrix(np.argmax(predicted_classes, axis=1), train_set.classes))

import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

DetaFrame_cm = pd.DataFrame(confusion_matrix(np.argmax(predicted_classes, axis=1), test.classes), test.class_indices , test.class_indices)
sns.heatmap(DetaFrame_cm, annot=True)

plt.show()
"""
# save results
hist_df = pd.DataFrame(history.history)
hist_df.to_csv(f"/content/drive/MyDrive/MA_code/traing_VGG_{i}.csv", sep=';')

predictions_df = pd.DataFrame(predicted_classes)
predictions_df.to_csv(f"/content/drive/MyDrive/MA_code/predictions_VGG_{i}.csv", sep=";")

labels_df = pd.DataFrame(valid_set.classes)
labels_df.to_csv(f"/content/drive/MyDrive/MA_code/labels_VGG_{i}.csv", sep=";")
"""