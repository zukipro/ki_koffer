# import dependencies
import numpy as np
import tensorflow as tf
import pandas as pd
import matplotlib.pyplot as plt
import tensorflow as tf
import shutil

# import deep learning dependencies
from tensorflow.keras.preprocessing.image import ImageDataGenerator , load_img ,img_to_array
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Activation, Dropout, Flatten, Dense, Conv2D, MaxPooling2D
from tensorflow.keras.callbacks import EarlyStopping
from sklearn.metrics import classification_report,confusion_matrix
from tensorflow.keras.optimizers import RMSprop
from tensorflow.keras.callbacks import EarlyStopping
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.applications.vgg16 import VGG16
from tensorflow.keras import layers, models
from keras.callbacks import EarlyStopping, ModelCheckpoint, ReduceLROnPlateau

# define image shape and batch size
image_shape = (150, 210, 3)
batch_size = 64

# path to processed data
processed_dir = "./training/"

# load images
image_gen = ImageDataGenerator(rescale=1 / 255.0,
                               rotation_range=20,
                               zoom_range=0.05,
                               width_shift_range=0.05,
                               height_shift_range=0.05,
                               shear_range=0.05,
                               horizontal_flip=True,
                               validation_split=0.20,
                               brightness_range=(0.5, 1.5))


train_set = image_gen.flow_from_directory(processed_dir,
                                          target_size=(150, 210),
                                          color_mode="rgb",
                                          batch_size=batch_size,
                                          class_mode='categorical',
                                          subset='training',
                                          shuffle=True,
                                          seed=42)

valid_set = image_gen.flow_from_directory(processed_dir,
                                          target_size=(150,210),
                                          color_mode="rgb",
                                          batch_size=batch_size,
                                          class_mode='categorical',
                                          subset='validation',
                                          shuffle=True,
                                          seed=42)


# load VGG 16 with imagenet weights
pre_trained_model = VGG16(input_shape=(150, 210, 3),
                          include_top=False,
                          weights='imagenet')

# freeze layers
pre_trained_model.trainable = True

# add classification layers
flatten_layer = layers.Flatten()
dense_layer_1 = layers.Dense(128, activation='relu')
prediction_layer = layers.Dense(24, activation='softmax')
dropout = layers.Dropout(0.2)

# add classification layers and vgg
model = models.Sequential([
    pre_trained_model,
    flatten_layer,
    dense_layer_1,
    dropout,
    prediction_layer
])

# compile and train model
model.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=0.0001),
              loss='categorical_crossentropy',
              metrics=['accuracy'])

earlyStopping = EarlyStopping(monitor='val_loss', patience=20, verbose=0, mode='min')
mcp_save = ModelCheckpoint('.mdl_wts.hdf5', save_best_only=True, monitor='val_loss', mode='min')
reduce_lr_loss = ReduceLROnPlateau(monitor='val_loss', factor=0.1, patience=7, verbose=1, min_delta=1e-4, mode='min')


history = model.fit(train_set,
                    validation_data=valid_set,
                    epochs=50,
                    callbacks=[earlyStopping, mcp_save, reduce_lr_loss])

model.save('train_color_2.h5')

model.save('/content/drive/MyDrive/Zuki/train_color_5.h5')

# plot training acc
acc = history.history['accuracy']
val_acc = history.history['val_accuracy']
loss = history.history['loss']
val_loss = history.history['val_loss']

epochs = range(len(acc))

plt.plot(epochs, acc, 'r', label='Training accuracy')
plt.plot(epochs, val_acc, 'b', label='Validation accuracy')
plt.title('Training and validation accuracy')
plt.legend(loc=0)
plt.figure()
plt.show()