# import dependencies
import numpy as np
import tensorflow as tf
import pandas as pd
import matplotlib.pyplot as plt
import tensorflow as tf
import shutil

# import deep learning dependencies
from tensorflow.keras.preprocessing.image import ImageDataGenerator , load_img ,img_to_array
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Activation, Dropout, Flatten, Dense, Conv2D, MaxPooling2D
from tensorflow.keras.callbacks import EarlyStopping
from sklearn.metrics import classification_report, confusion_matrix
from tensorflow.keras.optimizers import RMSprop
from tensorflow.keras.callbacks import EarlyStopping
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.applications.vgg16 import VGG16
from tensorflow.keras import layers, models
import cv2

# define image shape and batch size
image_shape = (150, 210, 3)
batch_size = 3
test_img = cv2.imread("./train_place/0/image_a_24.png")
print(test_img.shape)
# path to processed data
processed_dir = "./train_place/"

# load images
image_gen = ImageDataGenerator(rescale=1 / 255.0,
                               rotation_range=20,
                               zoom_range=0.05,
                               width_shift_range=0.05,
                               height_shift_range=0.05,
                               shear_range=0.05,
                               horizontal_flip=True,
                               validation_split=0.20,
                               brightness_range=(0.5, 1.5))

valid_gen = ImageDataGenerator(rescale=1 / 255.0)


train_set = image_gen.flow_from_directory(processed_dir,
                                          target_size=(150, 210),
                                          color_mode="rgb",
                                          batch_size=batch_size,
                                          class_mode='binary',
                                          subset='training',
                                          shuffle=True,
                                          seed=42)

valid_set = image_gen.flow_from_directory(processed_dir,
                                          target_size=(150, 210),
                                          color_mode="rgb",
                                          batch_size=batch_size,
                                          class_mode='binary',
                                          subset='validation',
                                          shuffle=True,
                                          seed=42)


model = models.Sequential()
model.add(layers.Conv2D(32, (3, 3), activation='relu', input_shape=(150, 210, 3)))
model.add(layers.MaxPooling2D((2, 2)))
model.add(layers.Conv2D(32, (3, 3), activation='relu'))
model.add(layers.MaxPooling2D((2, 2)))
model.add(layers.Conv2D(32, (3, 3), activation='relu'))
model.add(layers.Flatten())
model.add(layers.Dense(64, activation='relu'))
model.add(layers.Dense(1, 'sigmoid'))


# compile and train model
model.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=0.00001),
              loss='binary_crossentropy',
              metrics=['accuracy'])

history = model.fit(train_set,
                    validation_data=valid_set,
                    epochs=10)

model.save('../train_place_v4.h5')

# plot training acc
acc = history.history['accuracy']
val_acc = history.history['val_accuracy']
loss = history.history['loss']
val_loss = history.history['val_loss']

epochs = range(len(acc))

plt.plot(epochs, acc, 'r', label='Training accuracy')
plt.plot(epochs, val_acc, 'b', label='Validation accuracy')
plt.title('Training and validation accuracy')
plt.legend(loc=0)
plt.figure()
plt.show()

"""
# plot confusion matrix
probabilities = model.predict(test_set)

test_set.classes

predicted_classes = np.round(probabilities, decimals=0)
predicted_classes = predicted_classes.astype('int')
predicted_classes.shape
np.squeeze(predicted_classes)

print(confusion_matrix(predicted_classes, test_set.classes))

# save results
hist_df = pd.DataFrame(history.history)
hist_df.to_csv(f"/content/drive/MyDrive/MA_code/traing_VGG_{i}.csv", sep=';')

predictions_df = pd.DataFrame(predicted_classes)
predictions_df.to_csv(f"/content/drive/MyDrive/MA_code/predictions_VGG_{i}.csv", sep=";")

labels_df = pd.DataFrame(test_set.classes)
labels_df.to_csv(f"/content/drive/MyDrive/MA_code/labels_VGG_{i}.csv", sep=";")
"""