# import dependencies
import numpy as np
import tensorflow as tf
import pandas as pd
import matplotlib.pyplot as plt
import tensorflow as tf
import shutil

# import deep learning dependencies
from tensorflow.keras.preprocessing.image import ImageDataGenerator , load_img ,img_to_array
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Activation, Dropout, Flatten, Dense, Conv2D, MaxPooling2D
from tensorflow.keras.callbacks import EarlyStopping
from sklearn.metrics import classification_report,confusion_matrix
from tensorflow.keras.optimizers import RMSprop
from tensorflow.keras.callbacks import EarlyStopping
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.applications.vgg16 import VGG16 
from tensorflow.keras.applications.efficientnet_v2 import EfficientNetV2M 
from tensorflow.keras.applications.mobilenet import MobileNet 


from tensorflow.keras import layers, models
from keras.callbacks import EarlyStopping, ModelCheckpoint, ReduceLROnPlateau




for i in range(3):

    print(i)


    # define image shape and batch size
    image_shape = (150, 210, 3)
    batch_size = 8

    # path to processed data
    processed_dir = "./defect/"


    # load images
    image_gen = ImageDataGenerator(rescale=1 / 255.0,
                                rotation_range=20,
                                zoom_range=0.05,
                                width_shift_range=0.05,
                                height_shift_range=0.05,
                                shear_range=0.05,
                                horizontal_flip=True,
                                validation_split=0.10,  # Use 20% for validation
                                brightness_range=(0.5, 1.5))

    # split the data into training and validation sets
    train_set = image_gen.flow_from_directory(processed_dir,
                                            target_size=(80, 80),
                                            color_mode="rgb",
                                            batch_size=batch_size,
                                            class_mode='categorical',
                                            subset='training',  # Use 60% for training
                                            shuffle=True,
                                            seed=42)

    valid_set = image_gen.flow_from_directory(processed_dir,
                                            target_size=(80,80),
                                            color_mode="rgb",
                                            batch_size=batch_size,
                                            class_mode='categorical',
                                            subset='validation',  # Use 20% for validation
                                            shuffle=True,
                                            seed=42)



    # Load test data
    test_datagen = ImageDataGenerator(rescale=1. / 255)
    test_set = test_datagen.flow_from_directory('./test/',
                                                target_size=(80, 80),
                                                batch_size=batch_size,
                                                class_mode='categorical')

    # Number of images for training, validation, and testing
    train_images_count = train_set.samples
    valid_images_count = valid_set.samples
    test_images_count = test_set.samples

    print("Number of training images:", train_images_count)
    print("Number of validation images:", valid_images_count)
    print("Number of test images:", test_images_count)

    # Load VGG 16 with ImageNet weights
    pre_trained_model = VGG16(input_shape=(80, 80, 3),
                            include_top=False,
                            weights='imagenet',
                            classifier_activation='softmax')
    
    pre_trained_model2 = EfficientNetV2M(input_shape=(80, 80, 3),
                            include_top=False,
                            weights='imagenet',
                            classifier_activation='softmax') 
    
    pre_trained_model3  = MobileNet(input_shape=(80, 80, 3),
                            include_top=False,
                            weights='imagenet',
                            classifier_activation='softmax')

    # Freeze layers
    pre_trained_model.trainable = True
    pre_trained_model2.trainable = True
    pre_trained_model3.trainable = True

    models_available = [pre_trained_model, pre_trained_model2, pre_trained_model3]

    model_selected = models_available[i]

    # Add classification layers
    flatten_layer = layers.Flatten()
    dense_layer_1 = layers.Dense(128, activation='relu')
    prediction_layer = layers.Dense(3, activation='softmax')
    dropout = layers.Dropout(0.2)

    # Add classification layers and VGG
    model = models.Sequential([
        model_selected,
        flatten_layer,
        dense_layer_1,
        dropout,
        prediction_layer
    ])

    # Compile the model
    model.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=0.0001),
                loss='categorical_crossentropy',
                metrics=['accuracy'])

    # Define callbacks
    earlyStopping = EarlyStopping(monitor='val_loss', patience=20, verbose=0, mode='min')
    mcp_save = ModelCheckpoint('.mdl_wts.hdf5', save_best_only=True, monitor='val_loss', mode='min')
    reduce_lr_loss = ReduceLROnPlateau(monitor='val_loss', factor=0.1, patience=7, verbose=1, min_delta=1e-4, mode='min')

    # Train the model
    history = model.fit(train_set,
                        validation_data=valid_set,
                        epochs=50,
                        callbacks=[earlyStopping, mcp_save, reduce_lr_loss])

    # Evaluate the model on the test set
    test_loss, test_accuracy = model.evaluate(test_set)
    print("Test Loss:", test_loss)
    print("Test Accuracy:", test_accuracy)

    # Save the model
    model.save('../models/train_defect_v3.h5')

    # Plot training accuracy
    acc = history.history['accuracy']
    val_acc = history.history['val_accuracy']
    loss = history.history['loss']
    val_loss = history.history['val_loss']

    '''
    # Plotting the training and validation loss
    plt.figure(figsize=(8, 8))
    plt.subplot(2, 1, 1)
    plt.plot(acc, label='Training Accuracy')
    plt.plot(val_acc, label='Validation Accuracy')
    plt.legend(loc='lower right')
    plt.ylabel('Accuracy')
    plt.title('Training and Validation Accuracy')

    plt.subplot(2, 1, 2)
    plt.plot(loss, label='Training Loss')
    plt.plot(val_loss, label='Validation Loss')
    plt.legend(loc='upper right')
    plt.ylabel('Cross Entropy')
    plt.title('Training and Validation Loss')
    plt.xlabel('epoch')
    plt.show()
    '''