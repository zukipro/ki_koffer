import cv2
import pickle
import tensorflow as tf
import numpy as np

cap = cv2.VideoCapture('http://169.254.58.155:8000/stream.mjpg')
i = 0
while True:
  ret, frame = cap.read()
  cv2.imshow('Video', cv2.resize(frame, (int(frame.shape[1]/2), int(frame.shape[0]/2))))

  if cv2.waitKey(1) & 0xFF == ord('y'):  # save on pressing 'y'
        cv2.imwrite(f'img_defect{i}.png', frame)
        print(f'save image {i}')
        i = i+1

  if cv2.waitKey(1) == 27:
    exit(0)

