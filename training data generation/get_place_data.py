# import dependencies
import numpy as np
import tensorflow as tf
import pandas as pd
import matplotlib.pyplot as plt
import tensorflow as tf
import shutil
import cv2
import pickle
import time

from helper_functions import preprocess, draw_lines


cap = cv2.VideoCapture('http://169.254.242.185:8000/stream.mjpg')

matrix = [[818.50524436, 0.0, 716.31531958], [0.0, 818.50524436, 638.66807577], [0, 0, 1]]
distortion = [[1.87976406e+00],[1.40067691e+01], [-1.09650499e-03], [9.86541024e-04], [-2.31280843e+00],[2.19050876e+00],
              [1.50468667e+01],[9.80510634e-01],[0.00000000e+00],[0.00000000e+00],[0.00000000e+00],[0.00000000e+00],
              [0.00000000e+00],[0.00000000e+00]]

mtx = np.asarray(matrix)
dist = np.asarray(distortion)

open_file = open("../calibration_data/corner_points_1", "rb")
corner_points = pickle.load(open_file)
open_file.close()


map1, map2 = cv2.initUndistortRectifyMap(mtx, dist, None, None, (1232, 1640), cv2.CV_32FC1)
count = 0

while True:
    start = time.time()

    ret, frame = cap.read()

    frame = preprocess(map1, map2, corner_points, frame)
    img = frame.copy()

    if cv2.waitKey(1) & 0xFF == ord('y'):
        for i in range(1, 7):
            for j in range(1, 5):
                section = img[int((img.shape[0]) / 6 * (i - 1)):int((img.shape[0]) / 6 * i),
                              int((img.shape[1]) / 4 * (j - 1)):int((img.shape[1]) / 4 * j)]

                # section = cv2.resize(section, (60, 60), interpolation=cv2.INTER_AREA)
                #print(section.shape)
                #plt.imshow(section)
                #plt.show()
                section = cv2.resize(section, (210, 150), interpolation=cv2.INTER_AREA)
                #print(section.shape)
                #plt.imshow(section)
                #plt.show()
                cv2.imwrite(f"./training/train_place/image_h_{count}.png", section)
                print(f"saved image {count}")
                count += 1

    frame = draw_lines(frame)

    frame = cv2.rotate(frame, cv2.ROTATE_90_COUNTERCLOCKWISE)

    cv2.imshow('Video', cv2.resize(frame, (int(frame.shape[0]/2), int(frame.shape[1]/2)), interpolation=cv2.INTER_AREA))


    if cv2.waitKey(1) == 27:
        exit(0)
