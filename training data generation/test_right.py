import cv2
import numpy as np

img = cv2.imread("img_defect0.png")

while True:
    test = img.copy()[500:950, 500:1000]
    test = cv2.circle(test, (int(test.shape[1]/2), int(test.shape[0]/2)), 5, (0, 0, 255), 5)
    test = cv2.rotate(test, cv2.ROTATE_90_CLOCKWISE)
    #print(img.shape)
    #cv2.imshow("img", cv2.resize(test, (int(test.shape[1]/2), int(test.shape[0]/2))))
    cv2.imshow("img", test)

    if cv2.waitKey(1) == 27:
        exit(0)


