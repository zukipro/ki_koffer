import cv2
from helper_functions import *


cap = cv2.VideoCapture('http://169.254.58.155:8000/stream.mjpg')
i = 0

open_file = open("../calibration_data/corner_points_right", "rb")
corner_points_right = pickle.load(open_file)
open_file.close()

open_file = open("../calibration_data/box_points_right", "rb")
box_points_right = pickle.load(open_file)
open_file.close()

while True:
    ret, frame = cap.read()
    frame = frame[500:950, 500:1000]
    frame = four_point_transform(frame.copy(), corner_points_right)

    x1 = box_points_right[0, 0]
    x2 = box_points_right[3, 0]
    x3 = int(x1 + (x2 - x1)/3)
    x4 = int(x1 + (x2 - x1) / 3 * 2)

    y1 = box_points_right[0, 1]
    y2 = box_points_right[2, 1]
    y3 = int(y1 + (y2 - y1)/2)

    X = (x1, x2, x3, x4)
    Y = (y1, y2, y3)

    test = frame.copy()
    for x in X:
        for y in Y:
            cv2.rectangle(test,
                          (x - 35, y - 35),
                          (x + 35, y + 35),
                          (0, 255, 0),
                          4)



    if cv2.waitKey(1) & 0xFF == ord('y'):  # save on pressing 'y'
        for x in X:
            for y in Y:
                img = frame[y-40:y+40, x-40:x+40]

                cv2.imwrite(f'../training/defect/img_defect_4_{i}.png', img)
                print(f'save image {i}')
                i = i+1

    if cv2.waitKey(1) == 27:
        exit(0)

    cv2.imshow('Video', test)
